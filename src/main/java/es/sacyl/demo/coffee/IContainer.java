package es.sacyl.demo.coffee;

import es.sacyl.demo.custom.exception.NotEnoughException;

public interface IContainer {

	public boolean getPortion(Portion portion) throws NotEnoughException;  
    public int getCurrentVolume();  
    public int getTotalVolume();  
    public void refillContainer();

}
