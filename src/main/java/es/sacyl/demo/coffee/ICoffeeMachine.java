package es.sacyl.demo.coffee;

import es.sacyl.demo.custom.exception.NotEnoughException;

public interface ICoffeeMachine {

    public boolean makeCoffee(Portion portion) throws NotEnoughException;  
    public IContainer getCoffeeContainer();  
    public IContainer getWaterContainer();  

}
