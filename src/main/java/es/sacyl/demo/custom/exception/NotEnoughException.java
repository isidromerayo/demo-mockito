package es.sacyl.demo.custom.exception;

public class NotEnoughException extends Exception {

	public NotEnoughException(String text) {
		super(text);
	}

}
