package es.sacyl.demo.cavalr;

import java.util.Date;

public class UserManager {

	private UserService userService;
	
	public void saveUser(User user) {
		userService.saveUser(user);
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public User findUser(String userName) {
		return this.userService.findUserByName(userName);
	}

	public void getUserLastLogin(User user) {
	}

	public void updateUser(String user, Date lastLoginDate) {
		userService.updateUser(user, lastLoginDate);
	}
}
