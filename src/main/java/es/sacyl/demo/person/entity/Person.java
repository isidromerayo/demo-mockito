package es.sacyl.demo.person.entity;

public class Person {

	private final Integer id;
	private final String name;
	
	public Person(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
}
