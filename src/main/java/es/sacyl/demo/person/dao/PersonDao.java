package es.sacyl.demo.person.dao;

import es.sacyl.demo.person.entity.Person;

public interface PersonDao {

	public Person fetchPerson(Integer id);
	public void update(Person person);
}
