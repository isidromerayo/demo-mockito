package es.sacyl.demo.person.service;

import es.sacyl.demo.person.dao.PersonDao;
import es.sacyl.demo.person.entity.Person;

public class PersonService {

	private final PersonDao personDao;
	
	public PersonService(PersonDao personDao) {
		this.personDao = personDao;
	}
	
	public boolean update(Integer personId, String name) {
		Person person = personDao.fetchPerson(personId);
		if (person != null) {
			Person updatePerson = new Person(person.getId(), name);
			personDao.update(updatePerson);
			return true;
		} 
		return false;
	}
}
