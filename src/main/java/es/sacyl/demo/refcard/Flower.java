package es.sacyl.demo.refcard;

public class Flower {

	private int numberOfLeafs;
	
	public int getNumberOfLeafs() {
		return numberOfLeafs;
	}

	public void setNumberOfLeafs(int originalNumberOfLeafs) {
		this.numberOfLeafs = originalNumberOfLeafs;
	}

}
