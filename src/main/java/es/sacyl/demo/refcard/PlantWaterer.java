package es.sacyl.demo.refcard;

public class PlantWaterer {

	private WaterSource waterSource;
	private WateringScheduler wateringScheduler;
	
	public WaterSource getWaterSource() {
		return waterSource;
	}
	
	public PlantWaterer(WaterSource waterSource, WateringScheduler wateringScheduler) {
		this.waterSource = waterSource;
		this.wateringScheduler = wateringScheduler;
	}

	public WateringScheduler getWaterScheduler() {
		return wateringScheduler;
	}

}
