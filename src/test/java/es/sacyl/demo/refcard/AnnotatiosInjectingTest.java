package es.sacyl.demo.refcard;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.junit.Before;
import org.junit.BeforeClass;

public class AnnotatiosInjectingTest {

	@Mock
	private WaterSource waterSourceMock;
	
	@Spy
	private WateringScheduler wateringSchedulerSpy;
	
	@InjectMocks
	private PlantWaterer plantWaterer;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldInjectMocks() {
		assertNotNull(plantWaterer.getWaterSource());
		assertNotNull(plantWaterer.getWaterScheduler());
	}

}
