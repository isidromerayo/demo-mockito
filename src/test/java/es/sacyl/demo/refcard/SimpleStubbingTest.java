package es.sacyl.demo.refcard;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.BDDMockito.given;

public class SimpleStubbingTest {

    public static final int TEST_NUMBER_OF_LEAFS = 5;
	private static final int WANTED_DATE = 0;
	private static final int VALUE_FOR_WANTED_ARGUMENT = 4;
	private static final int ANY_OTHER_DATE = 4;

	@Test
	public void shouldReturnGivenValue() {
		// Arrange
		Flower flowerMock = mock(Flower.class);
		when(flowerMock.getNumberOfLeafs()).thenReturn(TEST_NUMBER_OF_LEAFS);
		
		// Act
		int numberOfLeafs = flowerMock.getNumberOfLeafs();
		
		// Assert
		assertEquals(TEST_NUMBER_OF_LEAFS, numberOfLeafs);
	}
	
	@Test
    public void shouldReturnGivenValueUsingBDDSemantics() {
		//given
		Flower flowerMock = mock(Flower.class);
		given(flowerMock.getNumberOfLeafs()).willReturn(TEST_NUMBER_OF_LEAFS);
		// when
		int numberOfLeafs = flowerMock.getNumberOfLeafs();
		
		// then
		assertEquals(TEST_NUMBER_OF_LEAFS, numberOfLeafs);
	}
	
	@Test
    public void shouldMatchSimpleArgument() {
	    WateringScheduler schedulerMock = mock(WateringScheduler.class);
	    given(schedulerMock.getNumberOfPlantsScheduledOnDate(WANTED_DATE)).willReturn(VALUE_FOR_WANTED_ARGUMENT);

	    int numberForWantedArgument = schedulerMock.getNumberOfPlantsScheduledOnDate(WANTED_DATE);
	    int numberForAnyOtherArgument = schedulerMock.getNumberOfPlantsScheduledOnDate(ANY_OTHER_DATE);

	    assertEquals(numberForWantedArgument, VALUE_FOR_WANTED_ARGUMENT);
	    assertEquals(numberForAnyOtherArgument, 0);  //default value for int

	}
	
	@Test
    public void shouldReturnLastDefinedValueConsistently() {
		WaterSource waterSource = mock(WaterSource.class);
	    given(waterSource.getWaterPressure()).willReturn(3, 5);
	    
	    assertEquals(waterSource.getWaterPressure(), 3);
	    assertEquals(waterSource.getWaterPressure(), 5);
	    assertEquals(waterSource.getWaterPressure(), 5);
	}

}
