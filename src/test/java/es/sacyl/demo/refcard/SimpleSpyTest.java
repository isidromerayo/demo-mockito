package es.sacyl.demo.refcard;

import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

public class SimpleSpyTest {

	private static final int ORIGINAL_NUMBER_OF_LEAFS = 5;
	private static final int NEW_NUMBER_OF_LEAFS = 3;

	@Test
	public void shouldStubMethodAndCallRealNotStubbedMethod() {
		Flower realFlower = new Flower();
		realFlower.setNumberOfLeafs(ORIGINAL_NUMBER_OF_LEAFS);
		Flower flowerSpy = spy(realFlower);
		willDoNothing().given(flowerSpy).setNumberOfLeafs(anyInt());
		
		flowerSpy.setNumberOfLeafs(NEW_NUMBER_OF_LEAFS); //stubbed - should do nothing
		
		verify(flowerSpy).setNumberOfLeafs(NEW_NUMBER_OF_LEAFS);
		assertEquals(flowerSpy.getNumberOfLeafs(), ORIGINAL_NUMBER_OF_LEAFS); // Value has not changed
		
	}
	
	@Mock(answer = Answers.RETURNS_SMART_NULLS)
	private PlantWaterer plantWatererMock;
	
	@Test
	public void changeMockDefaultReturnValue() {
		PlantWaterer plantWatererMock = mock(PlantWaterer.class, Mockito.RETURNS_DEEP_STUBS);
		given(plantWatererMock.getWaterSource().getWaterPressure()).willReturn(5);
	}
	
}
