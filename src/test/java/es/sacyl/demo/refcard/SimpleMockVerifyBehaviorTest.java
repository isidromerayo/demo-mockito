package es.sacyl.demo.refcard;

import org.junit.Test;
import org.mockito.InOrder;

import static org.mockito.Mockito.*;

public class SimpleMockVerifyBehaviorTest {
	
	@Test
	public void verifyBehavior() throws WaterException {
		// Arrange
		WaterSource waterSourceMock = mock(WaterSource.class);

		// Act
		waterSourceMock.doSelfCheck();
		waterSourceMock.getWaterPressure();
		waterSourceMock.getWaterTemperature();
		waterSourceMock.getWaterPressure();
		// Error
		// waterSourceMock.getWaterPressure();
		
		// Assert
		
		// By default, Mockito checks if a given method (with given arguments) was called once and only once
	    verify(waterSourceMock).doSelfCheck();
	    verify(waterSourceMock,atLeast(1)).getWaterTemperature();
	    verify(waterSourceMock,times(2)).getWaterPressure();
	}
	
	@Test
	public void verifyBehaviorNeverCall() throws WaterException {
		WaterSource waterSourceMock = mock(WaterSource.class);
		
		// waterSourceMock.doSelfCheck();
		verify(waterSourceMock,never()).getWaterTemperature();
	}
	
	@Test
    public void shouldVerifyInOrderThroughDifferentMocks() throws WaterException{
		// Arrange
		WaterSource waterSource1 = mock(WaterSource.class);
	    WaterSource waterSource2 = mock(WaterSource.class);
	    
	    // Act
	    waterSource1.doSelfCheck();
	    waterSource2.getWaterPressure();
	    waterSource1.getWaterTemperature();
	    
	    // Assert
	    InOrder inOrder=inOrder(waterSource1,waterSource2);
	    inOrder.verify(waterSource1).doSelfCheck();
	    inOrder.verify(waterSource2).getWaterPressure();
	    inOrder.verify(waterSource1).getWaterTemperature();
	}
}
