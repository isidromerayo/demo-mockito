package es.sacyl.demo.cavalr;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class UserManagerTest {

	
	@InjectMocks
    private UserManager userManager;
 
    @Mock
    private UserService userService;

	@Test
    public void shouldBeSaveUser() throws Exception {
        User user = new User("u1");
        userManager.saveUser(user);
 
        //Verify if saveUser was invoked on userService with given 'user' object.
        Mockito.verify(userService).saveUser(user);
 
        //Verify with Argument Matcher
        Mockito.verify(userService).saveUser(Mockito.<User>any());
    }
	
	@Test
	public void shouldBeCountNumberOfInteractions() throws Exception {
		userManager.findUser("user1");
		
		//Verify the number of interactions with mock
		Mockito.verify(userService, Mockito.times(1)).findUserByName("user1");
		//There was only one interaction with userService
		Mockito.verifyNoMoreInteractions(userService);
	}
	
	@Test
	public void shouldBeZeroInteractionsWithMock() throws Exception {
		User user = new User("user1", new Date());
		
		// call method where no call to userService will be made
		userManager.getUserLastLogin(user);
		Mockito.verifyZeroInteractions(userService);
		//Another way to check zero interactions
	    userManager.getUserLastLogin(user);
	}
	
	@Test
	public void shouldBeFindUser() throws Exception {
		User stubbedUser = new User("userAfterSave");
		Mockito.when(userService.findUserByName("user1")).thenReturn(stubbedUser);
		
		User user = userManager.findUser("user1");
		
		Mockito.verify(userService).findUserByName("user1");
		assertEquals("userAfterSave", user.getUserName());
	}
	
	@Test
	public void stubArrayList() throws Exception {
		//Mock the Arraylist
	    ArrayList mock = Mockito.mock(ArrayList.class);
	 
	    //Stub the values
	    Mockito.when(mock.get(0)).thenReturn("value1");
	    Mockito.when(mock.get(1)).thenReturn("value2");
	 
	    //Check the value at 0 & 1
	    assertEquals("value1", mock.get(0));
	    assertEquals("value2", mock.get(1));
	}
	
	@Test
	public void stubAndAnswer() throws Exception {
		//when method is invoked on mock, do some processing before return the stubbed object
		Mockito.when(userService.findUserByName(Mockito.anyString())).then(new Answer<User>() {
			public User answer(InvocationOnMock invocation) throws Throwable {
				assertNotNull(invocation.getArguments()[0]);
				return new User("UserCreatedByCallback");
			}
		});
		User user = userManager.findUser("user1");
		 
        assertEquals("UserCreatedByCallback", user.getUserName());
        
        Mockito.doAnswer(new Answer() {
        	public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
 
                return new User("UserCreatedByCallback2");
 
            }
 		}).when(userService).findUserByName(Mockito.anyString());
        
	}
	
	@Test
	public void argumentMatcher() throws Exception {
		Mockito.when(userService.findUserByName(Mockito.<String>any())).thenReturn(Mockito.<User>any());
		userManager.findUser("user");
		
		Mockito.verify(userService).findUserByName("user");
		
		//Another example
        userManager.updateUser("user", new Date());
 
        Mockito.verify(userService).updateUser(Mockito.eq("user"), Mockito.<Date>any());
	}
	
	@Test(expected=RuntimeException.class)
	public void throwException() throws Exception {
		Mockito.doThrow(new RuntimeException()).when(userService).saveUser(Mockito.<User>any());
		userManager.saveUser(Mockito.mock(User.class));
	}
	
}
