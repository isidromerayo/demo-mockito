package es.sacyl.demo.official_documentation;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import static org.mockito.Mockito.*;

public class ExamplesMockito {

	@Test
	public void verifyInteractions() {
		// mock creation - arrange
		List mockedList = mock(List.class);
		// using mock object - act
		mockedList.add("one");
		mockedList.clear();
		
		// selective and explicit verification - assert
		verify(mockedList).add("one");
		verify(mockedList).clear();
		// verifyNoMoreInteractions(mockedList);
	}
	
	@Test
	public void stubMethodsCalls() {
		// You can mock concrete class, no only interfaces
		LinkedList mockedList = mock(LinkedList.class);
		
		// stubbing - before execution
		when(mockedList.get(0)).thenReturn("first");
		
		// System.out.println(mockedList.get(0));
		// System.out.println(mockedList.get(99));
	}

}
