package es.sacyl.demo.person.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import es.sacyl.demo.person.dao.PersonDao;
import es.sacyl.demo.person.entity.Person;
import static org.mockito.Mockito.*;

public class PersonServiceTest {

	private PersonService personService;
	private PersonDao personDao;
	
	@Before
	public void before() {
		this.personDao = mock(PersonDao.class);
		this.personService = new PersonService(personDao);
	}
	
	@Test
    public void shouldUpdatePersonName() {
		// Arange
        Person person = new Person(1, "john");
        when(personDao.fetchPerson(1)).thenReturn(person);

        // Act
        boolean updated = personService.update(1, "joe");
        
        // Assert
        assertTrue(updated);

        verify(personDao).fetchPerson(1);

        ArgumentCaptor<Person> personCaptor = ArgumentCaptor
                .forClass(Person.class);
        verify(personDao).update(personCaptor.capture());
        Person updatedPerson = personCaptor.getValue();
        assertEquals("joe", updatedPerson.getName());

        verifyNoMoreInteractions(personDao);
    }
	
	@Test
	@Ignore
	public void shouldFailWithNiceMockitoError() {
		// Arrange
		when(personDao.fetchPerson(1)).thenReturn(null);
		// Act
		boolean updated = personService.update(1, "joe");
		// Assert
		assertFalse(updated);
		
		// org.mockito.exceptions.verification.NoInteractionsWanted: 
		// No interactions wanted here:
		//	-> at es.sacyl.demo.person.service.PersonServiceTest.shouldFailWithNiceMockitoError(PersonServiceTest.java:58)
		//	But found this interaction:
		//	-> at es.sacyl.demo.person.service.PersonService.update(PersonService.java:15)
		//	Actually, above is the only interaction with this mock.
		//		at es.sacyl.demo.person.service.PersonServiceTest.shouldFailWithNiceMockitoError(PersonServiceTest.java:58)
		// verify(personDao).fetchPerson(1);
		// notice no longer calling verify(personDao).fetchPerson(1);
        verifyNoMoreInteractions(personDao);
	}

}
